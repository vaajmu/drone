// I2C device class (I2Cdev) demonstration Arduino sketch for MPU6050 class
// 10/7/2011 by Jeff Rowberg <jeff@rowberg.net>
// Updates should (hopefully) always be available at https://github.com/jrowberg/i2cdevlib
//
// Changelog:
//      2013-05-08 - added multiple output formats
//                 - added seamless Fastwire support
//      2011-10-07 - initial release

/* ============================================
I2Cdev device library code is placed under the MIT license
Copyright (c) 2011 Jeff Rowberg

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
===============================================
*/

// I2Cdev and MPU6050 must be installed as libraries, or else the .cpp/.h files
// for both classes must be in the include path of your project
#include "I2Cdev.h"
#include "MPU6050.h"

// Arduino Wire library is required if I2Cdev I2CDEV_ARDUINO_WIRE implementation
// is used in I2Cdev.h
#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
    #include "Wire.h"
#endif

// class default I2C address is 0x68
// specific I2C addresses may be passed as a parameter here
// AD0 low = 0x68 (default for InvenSense evaluation board)
// AD0 high = 0x69
MPU6050 accelgyro;
//MPU6050 accelgyro(0x69); // <-- use for AD0 high

int16_t ax, ay, az;
int16_t gx, gy, gz;



// uncomment "OUTPUT_READABLE_ACCELGYRO" if you want to see a tab-separated
// list of the accel X/Y/Z and then gyro X/Y/Z values in decimal. Easy to read,
// not so easy to parse, and slow(er) over UART.
#define OUTPUT_READABLE_ACCELGYRO

// uncomment "OUTPUT_BINARY_ACCELGYRO" to send all 6 axes of data as 16-bit
// binary, one right after the other. This is very fast (as fast as possible
// without compression or data loss), and easy to parse, but impossible to read
// for a human.
//#define OUTPUT_BINARY_ACCELGYRO


#define LED_PIN 13
bool blinkState = false;
int offAX, offAY, offAZ, offRX, offRY, offRZ;
int cptAX, cptAY, cptAZ, cptRX, cptRY, cptRZ;
int axesCalibres = 0;

int updateOffset(int valeurMMU, int *cptAxesValides, int *cptOffValides, int marge) {
  int retour = 0, val = valeurMMU, multiplicateur = 1;

  // retour est la valeur à retourner : La valeur à ajouter à l'offset actuel

  // val est la valeur qu'on comparera, elle sera > 0 pour éviter de
  // faire deux fois la même chose

  // multiplicateur vaudra -1 si la valeur est négative, à la fin, on
  // multiplira le retour par le multiplicateur pour avoir le même
  // signe qu'à l'arrivée

  if(val < 0) {
    val = -val;
    multiplicateur = -multiplicateur;
  }

  if(*cptOffValides < 5  && *cptOffValides > 0 && val > marge) {
    // L'offset n'est plus bon
    *cptOffValides = 0;
  } else if(*cptOffValides > 0 && *cptOffValides < 5) {
    // L'offset était bien depuis pas assez longtemps
    (*cptOffValides)++;
  } else if(*cptOffValides == 5 && val < marge) {
    // L'offset est bien depuis assez longtemps
    (*cptAxesValides)++;
  }

  // L'offset n'était pas bien
  if(*cptOffValides == 0) {
    if(val > 10000) retour = -1000;
    else if(val > 1000) retour = -100;
    else if(val > 100) retour = -10;
    else if(val > 10) retour = -1;
    else (*cptOffValides)++;
  }

  retour *= multiplicateur;
  Serial.println("Ajoute " + String(retour) + " valide " + String(*cptOffValides) + " fois");
  return retour;
}

void setup() {
    // join I2C bus (I2Cdev library doesn't do this automatically)
    #if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
        Wire.begin();
    #elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
        Fastwire::setup(400, true);
    #endif

    // initialize serial communication
    // (38400 chosen because it works as well at 8MHz as it does at 16MHz, but
    // it's really up to you depending on your project)
    Serial.begin(9600);

    // initialize device
    Serial.println("Initializing I2C devices...");
    accelgyro.initialize();

    // verify connection
    Serial.println("Testing device connections...");
    Serial.println(accelgyro.testConnection() ? "MPU6050 connection successful" : "MPU6050 connection failed");

    offAX = offAY = offAZ = offRX = offRY = offRZ = 0;
    cptAX = cptAY = cptAZ = cptRX = cptRY = cptRZ = 0;

    // Calibrage automatique
    do {
      // read raw accel/gyro measurements from device
      accelgyro.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);

      // display tab-separated accel/gyro x/y/z values
      Serial.print("a/g:\t");
      Serial.print(ax); Serial.print("\t");
      Serial.print(ay); Serial.print("\t");
      Serial.print(az); Serial.print("\t");
      Serial.print(gx); Serial.print("\t");
      Serial.print(gy); Serial.print("\t");
      Serial.println(gz);

      axesCalibres = 0;
      offAX += updateOffset(ax, &axesCalibres, &cptAX, 100);
      offAY += updateOffset(ay, &axesCalibres, &cptAY, 100);
      offAZ += updateOffset(az, &axesCalibres, &cptAZ, 100);
      offRX += updateOffset(gx, &axesCalibres, &cptRX, 50);
      offRY += updateOffset(gy, &axesCalibres, &cptRY, 50);
      offRZ += updateOffset(gz, &axesCalibres, &cptRZ, 50);

      // if(ax > 1000) offAX -= 100;
      // else if(ax > 100) offAX -= 10;
      // else if(ax < 1000) offAX += 100;
      // else if(ax < 100) offAX += 10;
      // else axesCalibres++;
      
      // Update
      accelgyro.setXAccelOffset(offAX);
      accelgyro.setYAccelOffset(offAY);
      accelgyro.setZAccelOffset(offAZ);
      accelgyro.setXGyroOffset(offRX);
      accelgyro.setYGyroOffset(offRY);
      accelgyro.setZGyroOffset(offRZ);

      Serial.println("Maintenant : " +
		     String(offAX) + " " +
		     String(offAY) + " " +
		     String(offAZ) + " " +
		     String(offRX) + " " +
		     String(offRY) + " " +
		     String(offRZ));
    } while(axesCalibres != 6);

    Serial.println("Gyro calibre ! " +
		   String(offAX) + " " +
		   String(offAY) + " " +
		   String(offAZ) + " " +
		   String(offRX) + " " +
		   String(offRY) + " " +
		   String(offRZ));
    
    while (!Serial.available()); // Attendre un caractère
    Serial.read();
}

void loop() {

}
