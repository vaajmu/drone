#include <Servo.h>

#define MAXESC1 2000
#define MINESC1 702
#define ESC1 11

// Programme pour calibrer le second ESC 

// la valeur de MINESC1 est trouvée avec le programmer
// varierVitesseMoteur Avec la valeur min, à l'établissement du
// courant, le moteur doit se comporter comme sur la documentation. En
// ce qui nous concerne, une suite de bips

void setup() {
  Servo esc1;
  char c;

	      
  // Début prg
  Serial.begin(9600);
  esc1.attach(ESC1);
  Serial.println("DEBUT OK");

  // Attendre 1 seconde
  delay(1000);

  // Mettre au max
  esc1.writeMicroseconds(MAXESC1);

  // Brancher batterie à ce moment

  Serial.println("Connecter la batterie et attendre les bips\nEnsuite envoyer un char");

  // Attendre autorisation utilisateur
  while (!Serial.available());
  c = Serial.read();

  // Écrire la valeur minimale
  esc1.writeMicroseconds(MINESC1);

}

void loop() {  

}
