/* Utilisation du capteur Ultrason HC-SR04 */

#define US_TRIG 12
#define US_ECHO 8

// Programme pour afficher la distance entre le drone et la surface en
// dessous (en général le sol)

long lecture_echo;
long cm;

void setup()
{
  // Initialisation

  pinMode(US_TRIG, OUTPUT);
  digitalWrite(US_TRIG, LOW);

  pinMode(US_ECHO, INPUT);
  Serial.begin(9600);
}

void loop()
{
  // mettre US_TRIG à haut pendant 10 microsecondes
  digitalWrite(US_TRIG, HIGH);
  delayMicroseconds(10);

  // remettre ensuite US_TRIG à bas
  digitalWrite(US_TRIG, LOW);

  // récupérer la durée du signal de réponse
  lecture_echo = pulseIn(US_ECHO, HIGH);
  // transformer le résultat vers la bonne unitée
  cm = lecture_echo / 58;

  // Afficher la distance
  Serial.print("Distance m : ");
  Serial.println(lecture_echo);

  // Attendre avant la prochaine lecture
  delay(1000);
}
