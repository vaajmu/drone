#include <Servo.h>

#define MAXESC 2000
#define MINESC 703
#define ESC 11

// Programme pour changer le sens de rotation d'un moteur

// la valeur de MINESC1 est trouvée avec le programme
// varierVitesseMoteur Avec la valeur min, à l'établissement du
// courant, le moteur doit se comporter comme sur la documentation. En
// ce qui nous concerne, une suite de bips

void setup() {
  Servo esc;
  char c;
	    
	      
  // Début prg
  Serial.begin(9600);
  esc.attach(ESC);
  Serial.println("DEBUT OK");

  // Mettre au max pour entrer dans le mode de programmation
  esc.writeMicroseconds(MAXESC);

  // Attendre 1 seconde
  delay(1000);

  // Brancher batterie à ce moment
  Serial.println("Connecter la batterie");
  Serial.println("Attendre le signal W W W W et envoyer un char");

  // Attendre autorisation utilisateur
  while (!Serial.available());
  c = Serial.read();

  // Écrire la valeur minimale
  esc.writeMicroseconds(MINESC);

  // Et maintenant ?
  Serial.println("Si nécessaire, envoyer un char pour remettre le signal à haut");

  // Attendre autorisation utilisateur
  while (!Serial.available());
  c = Serial.read();

  // Écrire la valeur minimale
  esc.writeMicroseconds(MAXESC);

}

void loop() {  

}


