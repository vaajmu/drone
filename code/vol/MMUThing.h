#ifndef DEF_MMUTHING
#define DEF_MMUTHING

/* #include "I2Cdev.h" */
#include "MPU6050_6Axis_MotionApps20.h"

#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
#include "Wire.h"
#endif

#include "Axis.h"

class MMUThing
{

 public:
  MMUThing();

  /* Reset the internal values */
  void reset();

  /* Update the internal values */
  void update();

  /* Return true if the component act straight */
  bool isOK();

  /* Relative position from start point (or since last reset) */
  Axis getCurrentPosition();
  
  /* The speed */
  Axis getSpeed();

 private:
  
  /* current speed, current position and last position */
  Axis speed, current, last;
  
  // MPU control/status vars
  bool dmpReady;  // set true if DMP init was successful
  uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU
  uint8_t devStatus;      // return status after each device operation (0 = success, !0 = error)
  uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
  uint16_t fifoCount;     // count of all bytes currently in FIFO
  uint8_t fifoBuffer[64]; // FIFO storage buffer

  // orientation/motion vars
  Quaternion q;           // [w, x, y, z]         quaternion container
  VectorFloat gravity;    // [x, y, z]            gravity vector
  float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector

  /* The component */
  MPU6050 mmu;
  bool stateOK;
  unsigned long lastTime;

};

#endif
