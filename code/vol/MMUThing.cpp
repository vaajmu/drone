#include "MMUThing.h"
#include "Logs.h"

MMUThing::MMUThing() {
  Logs::getInstance().push("New MMUThing\n");
  stateOK = true;
  dmpReady = false;

  // join I2C bus (I2Cdev library doesn't do this automatically)
#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
  Wire.begin();
  TWBR = 24; // 400kHz I2C clock (200kHz if CPU is 8MHz)
#elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
  Fastwire::setup(400, true);
#endif

  mmu.initialize();
  stateOK = mmu.testConnection();
  if(!stateOK) Logs::getInstance().push("MMU connection failed\n");
  devStatus = mmu.dmpInitialize();
  mmu.setXGyroOffset(220);
  mmu.setYGyroOffset(76);
  mmu.setZGyroOffset(-85);
  mmu.setZAccelOffset(1788);

  if (devStatus == 0) {
    mmu.setDMPEnabled(true);
    mpuIntStatus = mmu.getIntStatus();
    dmpReady = true;
    packetSize = mmu.dmpGetFIFOPacketSize();
  } else {
    // ERROR!
    // 1 = initial memory load failed
    // 2 = DMP configuration updates failed
    // (if it's going to break, usually the code will be 1)
    stateOK = false;
    Logs::getInstance().push("MMU init failed\n");
  }

  update();
}

bool MMUThing::isOK() {
  return stateOK;
}

void MMUThing::reset() {
  current.setAxis(0, 0, 0, 0, 0, 0);
  speed.setAxis(0, 0, 0, 0, 0, 0);
}

void MMUThing::update() {
  unsigned long thisTime = millis();
  unsigned long delta = thisTime - lastTime;

  fifoCount = mmu.getFIFOCount();
  if(fifoCount >= packetSize) {
    // reset interrupt flag and get INT_STATUS byte
    mpuIntStatus = mmu.getIntStatus();
    fifoCount = mmu.getFIFOCount();

    // check for overflow (this should never happen unless our code is too inefficient)
    if ((mpuIntStatus & 0x10) || fifoCount == 1024) {
      // reset so we can continue cleanly
      mmu.resetFIFO();
      Logs::getInstance().push("FIFO Overflow\n");

      // otherwise, check for DMP data ready interrupt (this should happen frequently)
    } else if (mpuIntStatus & 0x02) {
      // wait for correct available data length, should be a VERY short wait
      while (fifoCount < packetSize) fifoCount = mmu.getFIFOCount();

      // Récupérer le dernier paquet
      while(fifoCount >= packetSize) {
        // read a packet from FIFO
        mmu.getFIFOBytes(fifoBuffer, packetSize);
        // track FIFO count here in case there is > 1 packet available
        // (this lets us immediately read more without waiting for an interrupt)
        fifoCount -= packetSize;
      }

      mmu.dmpGetQuaternion(&q, fifoBuffer);
      mmu.dmpGetGravity(&gravity, &q);
      mmu.dmpGetYawPitchRoll(ypr, &q, &gravity);
      last.setAxis(current); // copy
      current.setAxis(0, 0, 0, ypr[0] * 180/M_PI, ypr[1] * 180/M_PI, ypr[2] * 180/M_PI);

      speed.setAxis(0, 0, 0,
                    (current.getRX() - last.getRX())/delta, 
                    (current.getRY() - last.getRY())/delta, 
                    (current.getRZ() - last.getRZ())/delta);

      Logs::getInstance().push("mmu\tX " + String(current.getRX()) + "\t" +
                               "Y " + String(current.getRY()) + "\t" +
                               "Z " + String(current.getRZ()) + "\n");
    }
  }
  lastTime = thisTime;
}

Axis MMUThing::getSpeed() {
  return speed;
}

Axis MMUThing::getCurrentPosition() {
  return current;
}


