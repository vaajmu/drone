#include "ServoThing.h"
#include "Logs.h"

// Centre à 80°
// Gauche : 62°
// Droite : 103°
// On s'arrête à 15°

ServoThing::ServoThing(int pin, int min, int max) {
  Logs::getInstance().push("New ServoThing\n");
  this->pin = pin;
  this->min = min;
  this->max = max;
  this->angle = 0;

  servo.attach(this->pin, this->min, this->max);
  setAngle(0);
}

// Reçoit un angle entre -90° et 90°
void ServoThing::setAngle(int angle) {

  if(angle < MIN_ANGLE) {
    this->angle = MIN_ANGLE;
  } else if(angle > MAX_ANGLE) {
    this->angle = MAX_ANGLE;
  } else {
    this->angle = angle;
  }

  servo.write(map(this->angle + OFFSET, -90, 90, 0, 180));
}

void ServoThing::addAngle(int angle) {
  this->setAngle(this->angle + angle);
}

int ServoThing::getAngle() {
  return this->angle;
}

