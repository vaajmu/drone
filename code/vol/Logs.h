#ifndef DEF_LOGS
#define DEF_LOGS

#include "QueueList.h"

class Logs {

 public:
  static Logs &getInstance();
  /* Ajouter une ligne au log */
  void push(String str);

  /* Affiche la chaine */
  String pop();

 private:
  Logs();
  static Logs instance;
  QueueList<String> queue;
};

#endif
