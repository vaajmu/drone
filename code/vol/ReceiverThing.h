#ifndef DEF_RECEIVER
#define DEF_RECEIVER

/* #include "PinChangeInt.h" */
#include "Axis.h"
#include "Navigation.h"

/* #define NO_PORTB_PINCHANGES // to indicate that port b will not be used for pin change interrupts */
/* #define NO_PORTC_PINCHANGES // to indicate that port c will not be used for pin change interrupts */
/* #define NO_PORTD_PINCHANGES // to indicate that port d will not be used for pin change interrupts */

/* Arduino connections */
#define PIN_TR_X A0
#define PIN_TR_Y A1
#define PIN_TR_Z A2
#define PIN_ROT_Z A3
/* #define PIN_STATUS */

/* Index on arrays */
#define IND_TR_X 0
#define IND_TR_Y 1
#define IND_TR_Z 2
#define IND_ROT_Z 3
/* #define IND_STATUS */

enum State {
  DOWN, UP, GOING_DOWN, GOING_UP
};

/* Like a controleur */
class ReceiverThing {

 public:
  ReceiverThing(Navigation *nav); /* Must initialize the nav object */
  /* Look if the user want to do something like take off, land, move
     forward, make a stunt, ... and update nav object */
  void updateStatus();
  bool ping();			/* Return false if receiver not transmitting */

  /* void tr_x_up(); */
  /* void tr_x_down(); */

  /* void tr_y_up(); */
  /* void tr_y_down(); */

  /* void tr_z_up(); */
  /* void tr_z_down(); */

  /* void rot_z_up(); */
  /* void rot_z_down(); */

  static unsigned long last_tr_x_up, last_tr_y_up, last_tr_z_up, last_rot_z_up;
  static int last_tr_x, last_tr_y, last_tr_z, last_rot_z;
  char dernierChar;
  
 private:
  Navigation *nav;
  State state;
  int val_tr_x, val_tr_y, val_tr_z, val_rot_z;

};

#endif
