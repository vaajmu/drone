#ifndef DEF_ESCTHING
#define DEF_ESCTHING

#include <Servo.h>

/* 
   To change the motor value:
   use setValue or addValue with a value between 0 and 1 000 to adapt the
     motor speed
   use setInit or setNone to stop the motor
   */

class ESCThing
{

 public:
  ESCThing(int pin, int none, int init, int min, int max);

  void setSpeed(int speed);
  void addSpeed(int speed);
  void setValNone();
  void setValInit();

 private:
  Servo motor;			/* Physical motor associated to ESC */
  int value;			/* current value between none ant max_rot */
  int pin;			/* pin of ESC command */
  int val_none;			/* ESC don't detect a signal */
  int val_init;			/* low signal (motor init) */
  int val_min_rot;		/* min value for rotation */
  int val_max_rot;		/* max value accepted */

  void addValue(int value);
  void setValue(int value);     /* recieve only values between val_none and max_rot */
};

#endif
