#include "Axis.h"

Axis::Axis(){
  this->tX = 0;
  this->tY = 0;
  this->tZ = 0;
  this->rX = 0;
  this->rY = 0;
  this->rZ = 0;
}

Axis::Axis(int tX, int tY, int tZ, int rX, int rY, int rZ){
  this->tX = tX;
  this->tY = tY;
  this->tZ = tZ;
  this->rX = rX;
  this->rY = rY;
  this->rZ = rZ;
}

void Axis::setAxis(int tX, int tY, int tZ, int rX, int rY, int rZ){
  this->tX = tX;
  this->tY = tY;
  this->tZ = tZ;
  this->rX = rX;
  this->rY = rY;
  this->rZ = rZ;
}

void Axis::setAxis(Axis a){
  this->tX = a.getTX();
  this->tY = a.getTY();
  this->tZ = a.getTZ();
  this->rX = a.getRX();
  this->rY = a.getRY();
  this->rZ = a.getRZ();
}

void Axis::addAxis(int tX, int tY, int tZ, int rX, int rY, int rZ){
  this->tX += tX;
  this->tY += tY;
  this->tZ += tZ;
  this->rX += rX;
  this->rY += rY;
  this->rZ += rZ;
}

void Axis::addAxis(Axis a){
  this->tX += a.getTX();
  this->tY += a.getTY();
  this->tZ += a.getTZ();
  this->rX += a.getRX();
  this->rY += a.getRY();
  this->rZ += a.getRZ();
}

// Set values

void Axis::setTX(int v){
  this->tX = v;
}

void Axis::setTY(int v){
  this->tY = v;
}

void Axis::setTZ(int v){
  this->tZ = v;
}

void Axis::setRX(int v){
  this->rX = v;
}

void Axis::setRY(int v){
  this->rY = v;
}

void Axis::setRZ(int v){
  this->rZ = v;
}

// Adders

void Axis::addTX(int v){
  this->tX += v;
}

void Axis::addTY(int v){
  this->tY += v;
}

void Axis::addTZ(int v){
  this->tZ += v;
}

void Axis::addRX(int v){
  this->rX += v;
}

void Axis::addRY(int v){
  this->rY += v;
}

void Axis::addRZ(int v){
  this->rZ += v;
}

// Accessors

int Axis::getTX(){
  return this->tX;
}

int Axis::getTY(){
  return this->tY;
}

int Axis::getTZ(){
  return this->tZ;
}

int Axis::getRX(){
  return this->rX;
}

int Axis::getRY(){
  return this->rY;
}

int Axis::getRZ(){
  return this->rZ;
}

