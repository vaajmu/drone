#include "ReceiverThing.h"
#include "Logs.h"

void tr_x_up() {
  ReceiverThing::last_tr_x_up = millis();
}
void tr_x_down() {
  ReceiverThing::last_tr_x = millis() - ReceiverThing::last_tr_x_up;
}
void tr_y_up() {
  ReceiverThing::last_tr_y_up = millis();
}
void tr_y_down() {
  ReceiverThing::last_tr_y = millis() - ReceiverThing::last_tr_y_up;
}
void tr_z_up() {
  ReceiverThing::last_tr_z_up = millis();
}
void tr_z_down() {
  ReceiverThing::last_tr_z = millis() - ReceiverThing::last_tr_z_up;
}
void rot_z_up() {
  ReceiverThing::last_rot_z_up = millis();
}
void rot_z_down() {
  ReceiverThing::last_rot_z = millis() - ReceiverThing::last_rot_z_up;
}

ReceiverThing::ReceiverThing(Navigation *param) {
  Logs::getInstance().push("New ReceiverThing\n");
  this->nav = param;
  this->state = DOWN;

  // pinMode(PIN_TR_X, INPUT);
  // digitalWrite(PIN_TR_X, HIGH);

  // pinMode(PIN_TR_Y, INPUT);
  // digitalWrite(PIN_TR_Y, HIGH);

  // pinMode(PIN_TR_Z, INPUT);
  // digitalWrite(PIN_TR_Z, HIGH);

  // pinMode(PIN_ROT_Z, INPUT);
  // digitalWrite(PIN_ROT_Z, HIGH);

  // PCintPort::attachInterrupt(PIN_TR_X, &tr_x_up, RISING);
  // PCintPort::attachInterrupt(PIN_TR_Y, &tr_y_up, RISING);
  // PCintPort::attachInterrupt(PIN_TR_Z, &tr_z_up, RISING);
  // PCintPort::attachInterrupt(PIN_ROT_Z, &rot_z_up, RISING);
  // PCintPort::attachInterrupt(PIN_TR_X, &tr_x_down, FALLING);
  // PCintPort::attachInterrupt(PIN_TR_Y, &tr_y_down, FALLING);
  // PCintPort::attachInterrupt(PIN_TR_Z, &tr_z_down, FALLING);
  // PCintPort::attachInterrupt(PIN_ROT_Z, &rot_z_down, FALLING);
}

void ReceiverThing::updateStatus() {
  // Axis a;

  // if(last_tr_x != val_tr_x || last_tr_y != val_tr_y || last_tr_z != val_tr_z
  //    || last_rot_z != val_rot_z) {

  //   a.setAxis(val_tr_x, val_tr_y, val_tr_z, 0, 0, val_rot_z);

  //   val_tr_x = last_tr_x;
  //   val_tr_y = last_tr_y;
  //   val_tr_z = last_tr_z;
  //   val_rot_z = last_rot_z;

  //   nav.setTarget(a);
  // }


  // Code pour l'utilisation au clavier
  if(dernierChar != 0) {
    bool commandeReconnue = true;
    switch(dernierChar) {
    case 'p':			// Preparer au décollage
      nav->getPrepared();
      break;
    case 'u':			// Monter
      nav->setTarget(Axis(0, 0, 10, 0, 0, 0));
      break;
    case 'd':			// Descendre
      nav->setTarget(Axis(0, 0, -10, 0, 0, 0));
      break;
    case 's':			// Stabiliser
      nav->setTarget(Axis(0, 0, 0, 0, 0, 0));
      break;
    case 'c':			// Couper les moteurs
      nav->emmergencyCut();
      break;
    default:
      commandeReconnue = false;
      break;
    }
    if(commandeReconnue) {
      dernierChar = 0;
      Logs::getInstance().push("Commande prise en compte\n");
    }
    else
      Logs::getInstance().push("Commande non prise en charge\n");
  }
  
}

bool ReceiverThing::ping() {
  // return false;

  // Code pour l'utilisation au clavier
  return true;
}


