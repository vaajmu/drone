#include <Wire.h>
#include <Servo.h>

#include "PinChangeInt.h"
#include "MPU6050_6Axis_MotionApps20.h"
#include "I2Cdev.h"
#include "QueueList.h"

#include "Axis.h"
#include "ESCThing.h"
#include "ReceiverThing.h"
#include "Navigation.h"
#include "Logs.h"
#include "MMUThing.h"
#include "ServoThing.h"

// #include "pinChangeInt/PinChangeInt/PinChangeInt.h"

// globalement :

// un but à atteindre (donné par la télécommande en cours de vol ou
// par le programme pour le décollage)

// un coeff de stabilité (sous forme de marge pour la MMU ?) dynamique
// (faible pour le décollage, plus élevé le reste du temps)

// Si l'état du drone est dans les marges, aller vers le but
// (augmenter, diminuer la vitesse des moteurs vers le but)

// Si l'état n'est pas stable, alors stabiliser (augmenter/diminuer la
// vitesse des moteurs pour retourner dans les marges)

// calculer la moyenne d'erreur sur les quelques dernières valeurs. Si
// la moyenne est faible, on peux attendre un moment avant la
// prochaine itération de boucle. Sinon, il faudra recommencer
// rapidement

// En général, une itération dure entre 2500 et 5000 microsecondes

// Le drone a un état : Au sol, décollage, en vol ou atterissage

// A tout moment, si plus de communication avec la télécommande,
// couper l'alimentation des moteurs (en espérant que la vitesse des
// moteurs diminue progressivement)

// Classe Telecommande pour récupérer les ordres ?
// Demande d'atterrissage si bon statut et switch change ? ou réponds ?
// Donne la demande en fonction de la réception (vitesse, point fixe) ?

Navigation *nav;
ReceiverThing *receiver;
bool receiverStatus = false;

void cleanLogs() {
  String str = Logs::getInstance().pop();
  while (str != NULL) {
    Serial.print(str);
    str = Logs::getInstance().pop();
  }
}

void setup() {

  Serial.begin(9600);
  nav = new Navigation();
  receiver = new ReceiverThing(nav);

  pinMode(13, OUTPUT);
  if(!nav->mmuOK()) digitalWrite(13, HIGH);
  else digitalWrite(13, LOW);

  Logs::getInstance().push("LED OK\n");

  cleanLogs();

  while (!Serial.available()); // Attendre un caractère
  receiver->dernierChar = Serial.read();
  Serial.println("Char lu");
}

void loop() {
  // If active but no receiver signal, shut down the motors
  // If not active but get a signal from receiver, update status
  if(receiverStatus && !receiver->ping()) {
    nav->emmergencyCut();
    receiverStatus = false;
  } else if (!receiverStatus && receiver->ping()) {
    receiverStatus = true;
  }

  // Pricess if active
  if (receiverStatus) {
    receiver->updateStatus();
    nav->stabilize();
  }

  // Logs::getInstance().push("Fin loop\n");
  cleanLogs();

  // while (!Serial.available()); // Si un caractère
  // receiver->dernierChar = Serial.read();
  // Serial.println("Char lu");

  if (Serial.available()) { // Si un caractère
    receiver->dernierChar = Serial.read();
    Serial.println("Char lu");
  }
}


