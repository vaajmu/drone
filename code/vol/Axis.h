#ifndef DEF_AXIS
#define DEF_AXIS

class Axis {

 public:
  Axis();
  Axis(int tX, int ty, int tz, int rX, int rY, int rZ);

  void setAxis(int tX, int ty, int tz, int rX, int rY, int rZ);
  void addAxis(int tX, int ty, int tz, int rX, int rY, int rZ);

  void setAxis(Axis a);
  void addAxis(Axis a);

  void setTX(int v);
  void setTY(int v);
  void setTZ(int v);
  void setRX(int v);
  void setRY(int v);
  void setRZ(int v);

  void addTX(int v);
  void addTY(int v);
  void addTZ(int v);
  void addRX(int v);
  void addRY(int v);
  void addRZ(int v);

  int getTX();
  int getTY();
  int getTZ();
  int getRX();
  int getRY();
  int getRZ();

 private:
  int tX, tY, tZ, rX, rY, rZ;
};

#endif
