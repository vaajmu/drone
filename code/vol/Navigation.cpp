#include "Navigation.h"
#include "Logs.h"

Navigation::Navigation () : escL(VALUES_ESC_LEFT), escR(VALUES_ESC_RIGHT), 
			    escT(VALUES_ESC_TAIL), servo(PIN_SERVO, 800, 1040) {
  Logs::getInstance().push("New Navigation\n");
  // Default PID values to personalise
  kP = 1; kI = 0; kD = 0;
  target.setAxis(mmu.getCurrentPosition());
  lastTime = millis();
}

void Navigation::getPrepared() {
  escL.setSpeed(0);
  escR.setSpeed(0);
  escT.setSpeed(0);
}

void Navigation::setTarget(Axis t) {
  target.setAxis(t);
  error.setAxis(0, 0, 0, 0, 0, 0);
  iTerm.setAxis(0, 0, 0, 0, 0, 0);
}

void Navigation::emmergencyCut() {
  escL.setValInit();
  escR.setValInit();
  escT.setValInit();
  servo.setAngle(0);
}

void Navigation::stabilize() {
  Axis state, positionTarget, pid;
  int cmdL, cmdR, cmdT, cmdS;
  unsigned long time, delta;

  // Time update
  time = millis();
  delta = time - lastTime;

  // Actual position update
  mmu.update();
  state = mmu.getSpeed();

  // Error update
  oldError.setAxis(error);

  // We now allow the user to choose the Z axis translation and the 3 axis
  // rotation. The Z axis translation is the motor speed and the 3 axis rotation
  // are the drone speed

  // To do that, we set the error to 0 on the 3 translation axis so the
  // algorithm won't correct these axis and if the user use them, they would be
  // ignored. As the Z translation axis is the motor speed, there is no error,
  // just a value modification.
  error.setAxis(
    0,
    0,
    0,
    target.getRX() - state.getRX(),
    target.getRY() - state.getRY(),
    target.getRZ() - state.getRZ());

  // 'I' update
  iTerm.addAxis(error.getTX(), error.getTY(), error.getTZ(),
		error.getRX(), error.getRY(), error.getRZ());

  pid.setAxis(
    kP*error.getTX()+kI*iTerm.getTX()+kD*(oldError.getTX()-error.getTX())/delta,
    kP*error.getTY()+kI*iTerm.getTY()+kD*(oldError.getTY()-error.getTY())/delta,
    kP*error.getTZ()+kI*iTerm.getTZ()+kD*(oldError.getTZ()-error.getTZ())/delta,
    kP*error.getRX()+kI*iTerm.getRX()+kD*(oldError.getRX()-error.getRX())/delta,
    kP*error.getRY()+kI*iTerm.getRY()+kD*(oldError.getRY()-error.getRY())/delta,
    kP*error.getRZ()+kI*iTerm.getRZ()+kD*(oldError.getRZ()-error.getRZ())/delta);

  cmdL = -pid.getTX()/4 + pid.getTY()/2 + pid.getTZ()/3 + pid.getRX()/2 - pid.getRY()/4;
  cmdR = -pid.getTX()/4 - pid.getTY()/2 + pid.getTZ()/3 - pid.getRX()/2 - pid.getRY()/4;
  cmdT = pid.getTX()/2 + pid.getTY()/5 + pid.getTZ()/3 + pid.getRX()/5 + pid.getRY()/2 + pid.getRZ()/5;
  cmdS = -pid.getRZ()/10;

  // Add Z translation axis
  cmdL += target.getTZ();
  cmdR += target.getTZ();
  cmdT += target.getTZ();

  // Logs::getInstance().push("Commandes : " + String(cmdL) + " " + String(cmdR) + " " + String(cmdT) + " " + String(cmdS) + "\n");

  // Décommenté pour les tests
  // escL.addSpeed(cmdL);
  // escR.addSpeed(cmdR);
  // escT.addSpeed(cmdT);
  // servo.addAngle(cmdS);

  lastTime = time;
}

bool Navigation::mmuOK() {
  return mmu.isOK();
}

