#ifndef DEF_SERVOTHING
#define DEF_SERVOTHING

#define MIN_ANGLE -15
#define MAX_ANGLE 15
#define OFFSET -10              /* Si nécessaire */

#include <Servo.h>

class ServoThing {

 public:
  ServoThing(int pin, int min, int max);
  void setAngle(int angle);	/* 0 milieu, -90 à gauche, 90 à droite */
  void addAngle(int angle);
  int getAngle();

 private:
  int angle;			/* Angle of the servo between -90° and 90° */
  int pin;			/* Pin for servo command */
  int min, max;			/* Values (not angle) accepted by the servo */
  Servo servo;
};

#endif
