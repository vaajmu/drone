#ifndef DEF_NAVIGATION
#define DEF_NAVIGATION

#define VALUES_ESC_RIGHT 9, 690, 730, 800, 2000
#define VALUES_ESC_TAIL 10, 690, 730, 800, 2000
#define VALUES_ESC_LEFT 11, 690, 730, 800, 2000
#define PIN_SERVO 6

#include "Axis.h"
#include "MMUThing.h"
#include "ESCThing.h"
#include "ServoThing.h"

class Navigation {

 public:
  Navigation();

  void setTarget(Axis t);       /* Go to this speed */
  void stabilize();	     /* Adapt motor values for the current target */
  void emmergencyCut();	     /* Stop motors */
  bool mmuOK();		     /* False if MMU don't feel well */
  void getPrepared();	     /* get prepared to leave (turn on the motors) */

 private:
  unsigned long lastTime;	/* Just to get the delta time */
  int kP, kI, kD;		/* Coefficient PID */
  /* The target, set by setTarget (obviously) */
  Axis target;
  /* The last calculated error, the current error and the cumulated error (I)
     from PID */
  Axis oldError, error, iTerm;
  MMUThing mmu;
  /* USThing us; no more*/
  ESCThing escL, escR, escT;	/* left, right, tail */
  ServoThing servo;
};

#endif
