#include "ESCThing.h"
#include "Logs.h"

ESCThing::ESCThing(int pin, int none, int init, int min, int max) {
  Logs::getInstance().push("New ESCThing\n");
  this->value = val_init;
  this->pin = pin;
  this->val_none = none;
  this->val_init = init;
  this->val_min_rot = min;
  this->val_max_rot = max;
  this->motor.attach(pin);

  // Initialiser value sans accès direct
  this->setValInit();
}

void ESCThing::setSpeed(int speed){
  // Contraindre la valeur entre 0 et 1000, la mapper entre min_rot et max_rot
  // et appeller setValue (la valeur de setValue sera une valeur entre min_rot
  // et max_rot)
  setValue(
    map(
      constraint(speed, 0, 1000), 
      0, 1000, val_min_rot, val_max_rot)
  );
}

void ESCThing::addSpeed(int speed){
  // Mapper de 0 à 1000 vers min_rot à max_rot et appeller addValue. La valeur
  // de addValue ne sera pas bornée, en particulier, elle peut être négative
  addValue(map(speed, 0, 1000, val_min_rot, val_max_rot));
}

void ESCThing::addValue(int value){
  // Si le moteur ne tournait pas, contraint la valeur entre min_rot et
  // max_rot. Sinon ajoute la valeur et contraint le résultat entre min_rot et
  // max_rot.
  if(this->value < val_min_rot)
    this->setValue(
      constraint(value, val_min_rot, val_max_rot)
    );
  else
    this->setValue(
      constraint(this->value + value, val_min_rot, val_max_rot)
    );
}

void ESCThing::setValNone(){
  this->setValue(val_none);
}

void ESCThing::setValInit(){
  this->setValue(val_init);
}

// Si appel depuis setSpeed, la valeur est entre min_rot et max_rot, donc
// valide. Pas d'appel depuis addSpeed. Si appel depuis addValue, la valeur est
// entre min_rot et max_rot. Si appel depuis setValNone ou setValInit, la valeur
// sera val_none ou val_init.
void ESCThing::setValue(int value){
  this->motor.writeMicroseconds(value);
  this->value = value;
}
