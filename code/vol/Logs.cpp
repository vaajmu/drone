#include "Logs.h"

Logs Logs::instance = Logs();

Logs::Logs() {
}

Logs &Logs::getInstance() {
  return instance;
}

void Logs::push(String str) {
  queue.push("LOGS::"+str);
}

String Logs::pop() {
  if(queue.isEmpty()) return NULL;
  return queue.pop();
}

