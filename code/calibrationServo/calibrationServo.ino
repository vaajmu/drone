#include <Servo.h>

#define SERVO_PIN_CMD 6

// Programme pour rechercher la plage de valeur du servomoteur

// Envoyer un caractère quelconque pour tester la valeur suivante
// Envoyer s (select) si le servo commence à se déplacer
// Envoyer s (select) de nouveau si le servo arrête de se déplacer

void setup() {

  // Déclarations
  Servo servo;
  char c;
  int val = 0;
  int pas = 20;
  int min = 0, max = 0;

  Serial.begin(9600);
  Serial.println("DEBUT OK\nEn attente");
  
  while(!Serial.available());	// Attendre un caractère pour commencer la recherche
  c = Serial.read();

  while(min == 0 || max == 0) {	// Tant que les deux valeurs n'ont pas été trouvés
    Serial.println(val);	// Afficher la valeur courante
    
    servo.attach(6, val, val);	// Initialiser le servo avec la valeur
    servo.write(0);
    
    while(!Serial.available());	// Attendre avant la valeur suivante
    c = Serial.read();

    if(c == 's') {		// Si l'utilisateur selectionne cette valeur
        if(min == 0) min = val;	// modifier la bonne variable
        else max = val;
    }

    servo.detach();		// Relacher le servo
    val += pas;			// Incrémenter la valeur
  }

  Serial.println("FIN\nTESTS !");

  while(!Serial.available());
  c = Serial.read();

  // Utiliser la plage trouvée
  servo.attach(6, min, max);
  servo.write(0);
  while(!Serial.available());
  c = Serial.read();
  servo.write(180);
  while(!Serial.available());
  c = Serial.read();

}

void loop() {

}
