#include <Servo.h>

#define MAXESC1 2000
#define MINESC1 700
#define ESC1 9
#define PAS 1

 // Programme pour faire varier la puissance d'un moteur.
 // Utile pour déterminer la valeur minimum à appliquer au moteur pour l'initialiser


void setup() {
  Servo esc1;
  int val = MINESC1;
  char c;

  Serial.begin(9600);
  Serial.println("DEBUT OK");

  // initialiser la variable ESC
  esc1.attach(ESC1);

  // Attendre 1 seconde
  // delay(1000);

  // Affecter la valeur minimale à l'ESC
  esc1.writeMicroseconds(MINESC1);
  Serial.println("MIN OK");

  Serial.println(val);		// Afficher la valeur initiale

  while(1) {			// Jusqu'à la fin

    while (!Serial.available()); // Attendre un caractère
    c = Serial.read();
    if(c == 'p') {		// Si p : augmenter la vitesse
      val += PAS;
      esc1.writeMicroseconds(val);
    }
    else if(c == 'm') {		// Si m : diminuer la vitesse
      val -= PAS;
      esc1.writeMicroseconds(val);
    }
    else			// Sinon, message d'erreur
      Serial.println("non reconnu");
    
    Serial.println(val);
  }  

}

void loop() {  

}
