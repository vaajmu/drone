#include <Servo.h>

#define MAX_SIGNAL 2000
#define MIN_SIGNAL 703
#define MIN_SIGNAL_ROTATION 900
#define ESC1 9
#define ESC2 10
#define ESC3 11
#define PAS 10

// Programme pour faire varier la vitesse des 3 moteurs en même temps

void setup() {
  Servo esc1, esc2, esc3;
  int val1 = MIN_SIGNAL, val2 = MIN_SIGNAL, val3 = MIN_SIGNAL;
  bool modifie;
  char c;

  // Établir la communication et initialiser les variables moteurs
  Serial.begin(9600);
  esc1.attach(ESC1);
  esc2.attach(ESC2);
  esc3.attach(ESC3);

  // Envoyer la valeur minimum sur les moteurs. Nécéssaire pour ne pas
  // entrer en mode programmation, ni partir trop vite ou sans le
  // vouloir et pour ne pas partir à fond
  esc1.writeMicroseconds(val1);
  esc2.writeMicroseconds(val2);
  esc3.writeMicroseconds(val3);
  Serial.println("MIN OK");

  // Attendre 1s
  delay(1000);
  Serial.println("DEBUT OK");

  while(1) {

    while (!Serial.available()); // Attendre un ordre de modification
    c = Serial.read();
    modifie = true;

    switch(c) {
    case 'a':
      val1 -= PAS;
      break;
    case 'z':
      val1 += PAS;
      break;
    case 'e':
      val2 -= PAS;
      break;
    case 'r':
      val2 += PAS;
      break;
    case 's':
      val3 -= PAS;
      break;
    case 'd':
      val3 += PAS;
      break;
    case 'p':
      val1 += PAS;
      val2 += PAS;
      val3 += PAS;
      break;
    case 'm':
      val1 -= PAS;
      val2 -= PAS;
      val3 -= PAS;
      break;
    case 'l':
      val1 = val2 = val3 = MIN_SIGNAL_ROTATION;
      break;
    case 'x':
      val1 = val2 = val3 = MIN_SIGNAL;
      break;
    default:
      Serial.println("non reconnu");
      modifie = false;
    }
    
    if(modifie) {
      esc1.writeMicroseconds(val1);
      esc2.writeMicroseconds(val2);
      esc3.writeMicroseconds(val3);
      Serial.print(val1);
      Serial.print(" ");
      Serial.print(val2);
      Serial.print(" ");
      Serial.println(val3);
    }
  }

}

void loop() {  

}
