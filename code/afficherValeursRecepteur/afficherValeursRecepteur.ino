#define PIN_RECEPTEUR A0

volatile unsigned long temps;

void setup() {
  int tab = 0, old, i, tmp;
  
  Serial.begin(9600);

  Serial.println("DEBUT OK");
  
//  old = analogRead(PIN_RECEPTEUR);
  old = pulseIn(PIN_RECEPTEUR, HIGH);
//  old = digitalRead(PIN_RECEPTEUR);
  while(1) {
//    tmp = analogRead(PIN_RECEPTEUR);
    tmp = pulseIn(PIN_RECEPTEUR, HIGH);
//    tmp = digitalRead(PIN_RECEPTEUR);
    if(tmp != old) {
      for(i = 0; i < tab; i++)
        Serial.print(" ");
      Serial.println(tmp);
      old = tmp;
      ++tab %= 100;
    }
  }

}

void loop() {
  // put your main code here, to run repeatedly:

}
