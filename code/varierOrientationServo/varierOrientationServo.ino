#include <Servo.h>

#define SERVO_PIN_CMD 6

// Programme pour tester le servomoteur

Servo servo;
int val;

void setup() {

  // Initialisation
  Serial.begin(9600);
  servo.attach(SERVO_PIN_CMD, 800, 1040);
  Serial.println("OK");

  // Initialiser au centre
  val=90;
  servo.write(val);
}

void loop() {
  char c;

  // Attendre l'utilisateur pour mettre à -90°
  while(!Serial.available());
  c = Serial.read();
  switch(c) {
  case 'm':                     // moins
    val--;
    break;
  case 'p':                     // plus
    val++;
    break;
  default:
    val=90;
    break;
  }
  Serial.println(val);
  servo.write(val);
}
