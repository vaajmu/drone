Discussion entre bytemare et vaajmu :

- la différence entre commit et push est simple mais repose sur un
  principe. Dans git (et à priori chez les autres aussi) tu fait un commit quand
  tu as ajouté une valeur a ton projet, par exemple tu as résolu un bug ou
  ajouté une fonctionnalité, tu DOIS faire un commit, si pour résourdre ce bug
  tu as modifié plusieurs fichiers, ils seront ajoutés ensemble, inversement, si
  tu as résolu 2 bugs, il faut faire 2 commits, même si tu n'as modifié qu'un
  seul fichier. Une des raisons, c'est pck le commit a un message, et un message
  concerne une modification

- et si je résous deux bugs d'un coup, faut quand même faire deux commit avec
  des messages différents mais le meme code ?

- tout est une histoire de point de vue, si tu résouds 2 bugs en même temps, tu
  résouds en faite une seule erreur dans ton code, par exemple si tu as oublié
  un moins dans le calcul de stabilisation de drone, en le corrigeant, tu
  résouds le bug du drone qui se crashe, le bug du drone qui ne vole pas droit
  et le bug du drone qui veux pas voler [edit:]décoller[/edit]. Dans ce cas là,
  tu fais un seul commit et ton message c'est "correction du calcul de
  stabilité"

_________________________________________

Commandes git :
-----------------------------------------------------------------------------
Configuration (--global affecte tout les dépots qui existent et existeront) Si
sans --global, doit s'executer dans un sous dossier d'un projet git (puisque
local à un projet). A faire une fois seulement

Ajouter de la couleur
git config --global color.ui true git config

Parfois, git a besoin d'ouvrir un éditeur, généralement vim par défaut
git config --global core.editor emacs

Identifiants envoyés à la soumission (doit correspondre au compte sur gitlab)
git config user.name monPseudo
git config user.email monEmail
------------------------------------------------------------------------------
------------------------------------------------------------------------------
Pour suivre un projet existant :
git clone 'protocole''cible'

Par exemple avec https (aurait pu être ssh par exemple)
git clone https://gitlab.com/vaajmu/drone.git
------------------------------------------------------------------------------
------------------------------------------------------------------------------
Pour voir les commits :
git log
Affiche la date, l'auteur, le hash (id du commit) et le message
------------------------------------------------------------------------------
------------------------------------------------------------------------------
Pour voir les modifications apportées à un fichier:
git diff pathFichier
Affiche + ..... pour les ajouts de ligne et - ....... Pour les suppressions

Pour voir les différences avec un commit plus ancien :
git diff hashCommit pathFichier
Le hash du commit peut être retrouvé avec git log

Pour voir toutes les modifications faites :
git diff
Si hash en argument, montre les différences du commit
------------------------------------------------------------------------------
------------------------------------------------------------------------------
L'index est une zone tampon dans laquelle on ajoute les fichiers un a un qui
feront partie d'un commit. On ne peux ajouter que des fichiers qui ont étés
modifiés

Pour ajouter un fichier à l'index :
git add pathFichier

Pour ajouter à l'index les fichiers de ce répertoire :
git add .

Pour ajouter à l'index tous les fichiers :
git add *
------------------------------------------------------------------------------
------------------------------------------------------------------------------
Un commit est un ensemble de modifications

Pour faire un commit : (ouvre un éditeur pour entrer le message)
git commit
Attention, le message ne résume pas les modifications, il y a une commande git
pour voir ce qui a changé. Le message résume "Pourquoi" est-ce qu'on a fait ces
modifications. Il faut se justifier, pck quand ces modifications provoqueront un
bug dans 10ans, qu'on verra qui a fait ces lignes de code (merci git, c'est
facile à savoir), il faudra savoir "Pourquoi" est-ce que le mec a fait ça

Faire un commit et mettre le message en ligne de commande
git commit -m "message"
------------------------------------------------------------------------------
------------------------------------------------------------------------------
Pour déplacer/supprimer un fichier:
git mv src dst
git rm pathFichier
Important, la commande bash ne tiens pas à jour le dépot git. Pour le
déplacement par exemple, il faut l'indiquer à git pour que la modification
s'applique aussi chez les autres
------------------------------------------------------------------------------
------------------------------------------------------------------------------
Pour mettre à jour le dépot avec les modifications soumises par ls autres :
git pull
------------------------------------------------------------------------------
------------------------------------------------------------------------------
Pour afficher les modifications depuis le dernier commit :
git status
Affiche les fichiers modifiés, les fichiers créés, les fichiers ajoutés à
l'index, ...
------------------------------------------------------------------------------
------------------------------------------------------------------------------
Pour rendre public les commits
git push origin 'brancheActuelle'
La branche actuelle est master si elle n'a pas été changée

Pour éviter des erreurs, on peux se souvenir de la dernière branche utilisée :
git push -u origin 'brancheActuelle'
Et à la prochaine utilisation :
git push
------------------------------------------------------------------------------
------------------------------------------------------------------------------
Pour annuler des modifications, aller voir :
http://git-scm.com/book/fr/v1/Les-bases-de-Git-Annuler-des-actions
ou plus complet :
https://fr.atlassian.com/git/tutorials/undoing-changes/git-reset
Attention, avec certaines commandes/arguments, une annulation peut supprimer irrémédiablement
des fichiers
