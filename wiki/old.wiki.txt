==== Maverick ====

Daniel Bourdrez\\
Antoine Lohéac\\
Aymeric Barbin\\

=== Présentation de l'équipe ===

L'équipe est constitué de trois étudiants en Master Informatique à
l'UPMC. Antoine Lohéac, en première année de SAR, Aymeric Barbin et
Daniel Bourdrez (chef d'équipe), tous deux en première année de SFPN.

=== Type de drone à construire ===

== Structure du Drone ==

Notre but est d'avoir un drone véloce avec une stabilité assurée par
un équilibrage automatique. Nous voulons un drone agile afin d'être
capable d'accomplir des acrobaties que nous lui aurons "apprises"
(mouvements et trajectoires pré-enregistrés et automatisés).  Nous
avons donc opté pour un Tricopter, plus léger qu'un Quadcopter et au
mouvement plus fluide.  Le châssis sera en fibre de carbone et
fabriqué par Turnigy (le Talon Tricopter V1.0).

== La mécanique du drone ==
Le choix des hélices et des moteurs a été fait dans l'optique d'un drone agile.\\
Hélices : plusieurs modèles pour tester la stabilité : 12"6, 9"6.5" et 10"8".\\
Moteurs : Turnigy D2836/9 950KV Brushless Outrunner

== L’électronique du drone ==
Contrôleur de vol : Arduino Uno\\
Carte de distribution : Hobby King Quadcopter Power Distribution Board Lite à 4 sorties\\
Centrale Inertielle : Gyroscope et Accéléromètre 3 axes (MPU-6050)
Capteur ultra-sons : HC-SR04
Voltmètre
Servomoteur : BMS-385DMAX Digital Servo (Metal Gear)
Batteries : ZIPPY Compact 5800mAh 4S 25C LiPo (x3)
Télécommande : Turnigy 9X 9Ch Transmitter w/ Module & 8ch Receiver (Mode 1) (v2 Firmware)
Batterie télécommande : Turnigy 9XR Safety Protected 11.1v (3s) 2200mAh 1.5C Transmitter Pack

L’informatique du drone (la programmation)

Pour l'informatique, c'est de l'artisanal !
Quelques soucis de temps en temps (nos débuts sur l'embarqué)

Nous nous sommes d'abord dit que nous allions prendre un pilote déjà
tout fait, puis nous avons accepté le challenge de faire tout le code
nous même. Il se pourrait que nous nous inspirerons de logiciels
open-source (comme ArduPilot) en cas de panne.  Nous comptons ainsi
stabiliser le drone, préenregistrer des mouvements d'acrobaties voir
peut-être le rendre autonome si le temps nous le permet.

Choix des composants

Hélices

Afin de conserver une vélocité élevée, nous avons décidé de réduire la
longueur des hélices pour réduire la consommation à vitesse de
rotation élevée tout en augmentant le pas pour gagner en vitesse de
déplacement.  Nous utiliserons alors plusieurs configuration afin de
trouver un bon équilibre et tester notre PID.  Pour les premiers test
nous utiliserons des hélices 12"6", puis nous essaierons des 9"6.5" et
des 10"8".

Moteurs

Pour un comportement agile, nous avons décidé d'un montant de 11.000 à
13.000 tours par minute avec de telles hélices. Avec un moteur à 950
KV et une alimentation à 14.7 V (LiPo 4S) nous pouvons atteindre 13965
tr/min, variable par l'ESC. Cela nous donne des indications pour l'ESC
et les batteries.  Electronic Speed Controller (ESC) Le moteur
consommant à 23.3 A maximum, nous avons opté pour un ESC avec BEC qui
supporte cela (30 A et 40A en BURST) et qui peut alimenter la carte de
vol en même temps.

Servo

Ce servo a été choisi car utilisé par de nombreux amateurs sur
plusieurs sites, nous pensons donc que cela peut être un bon choix. Il
a un plus grand moment et une plsu grande vitesse pour un poids et un
prix inférieur à d'autres servos comparables.

Carte de Vol

Nous nous posons un défi de programmation pour ce projet, de plus
l'Arduino Uno semble être bien connue du PMCLab, nous espérons donc
avoir un soutien technique en cas de problème. Les possibilité en
termes de programmation sont très intéressantes et il y a une
communauté très ouvertes avec qui nous espérons partager notre
travail.

Capteurs

Les composants choisis nous donnent les informations dont nous avons
besoin pour les statistiques du vol, qui nous permettrons d'effectuer
des calculs et de formaliser des mouvements.  Carte de distribution 4
sorties pour trois moteurs, ce qu'il y a de plus basique.

Batterie

On a une tension maximale possible de 14.7 V (donc 4 cellules) avec un
ampérage à 23.3 A max par moteur. Pour assurer une durée de vol
convenable tout en fournissant correctement les moteurs, nous optons
pour une capacité de 5800 mAh, pour laquelle une capacité de décharge
de 25C est largement suffisante.

Télécommande

Turnigy étant une vraie référence dans le domaine, nous nous sommes
décidés pour ce modèle car il contient le récepteur compatible avec la
boîte et tourne sur un firmware mit à jour.

Liste des composants, prix et lien d'achat:
(seuls les prix totaux en gras sont à prendre en compte)

Châssis :
63.99 €
http://hobbyking.com/hobbyking/store/__25437__Turnigy_Talon_Tricopter_V1_0_Carbon_F
iber_Frame.html
Hélices:
21,16 €
- 9"6.5" : 2.02€ * 3 = 6.06 €
http://hobbyking.com/hobbyking/store/__16062__Master_Airscrew_Formula_One_Propeller_9x6_5
.html
- 10"8" : 1.99€ * 3 = 5.98 €
http://hobbyking.com/hobbyking/store/__8053__Master_Airscrew_propeller_10x8.html
- 12"6" : 4.56€ * 2 = 9.12 €
http://hobbyking.com/hobbyking/store/__27465__GWS_EP_Propeller_HD_1260_305_x_152mm_Gr
ey_6pcs_set_.html
Moteurs x 3 : 33,57 €
http://www.hobbyking.com/hobbyking/store/uh_viewItem.asp?idProduct=34227
Servomoteur : 16.40 €
http://www.hobbyking.com/hobbyking/store/__8761__BMS_385DMAX_Digital_Servo_Metal_Gear_
4_2kg_15sec_16_5g.html
ESC x 3:
28,68 €
http://www.hobbyking.com/hobbyking/store/__13429__HobbyKing_30A_BlueSeries_Brushless_Spe
ed_Controller.html
Batteries x 2: 71,74 €
http://www.hobbyking.com/hobbyking/store/__21381__ZIPPY_Compact_5800mAh_4S_25C_Lipo_P
ack.html
Carte de distribution : 3.11 €
http://hobbyking.com/hobbyking/store/__27036__Hobby_King_Quadcopter_Power_Distribution_B
oard_Lite_.html
Télécommande et batterie : 58,38 €
http://www.hobbyking.com/hobbyking/store/__8991__Turnigy_9X_9Ch_Transmitter_w_Module_8c
h_Receiver_Mode_1_v2_Firmware_.html
http://www.hobbyking.com/hobbyking/store/__31315__Turnigy_9XR_Safety_Protected_11_1v_3s_
2200mAh_1_5C_Transmitter_Pack.html
Carte de controle:
10.32 €
http://hobbyking.com/hobbyking/store/__23767__Arduino_Uno_Atmel_Atmega_328_PU.html
Gyro/Accel : 3.37 €
http://www.electrodragon.com/product/mpu-6050-three-axis-acceleration-gyroscope-6-dof-module
Capteur Ultrasons:
1.68 €
http://www.electrodragon.com/product/ultrasonic-proximity-sensor/
Voltmètre:
1.53 €
http://www.electrodragon.com/product/two-wires-voltmeter-3-2-30v/
Zip Ties:
1.78 €
http://www.hobbyking.com/hobbyking/store/__46892__Cable_Ties_160_x_2_5mm_Black_100pcs_.
html
TOTAL :
315,71€
Ce qui nous laisse de la marge pour des composants oubliés et/ou nos extensions prévues.

Sources

Nous avons beaucoup appris par les vidéos de FliteTest.  Nous nous
sommes inspirés des recommandations des constructeurs sur le site
d'HobbyKing et des commentaires utilisateurs ainsi que de nombreux
amateurs sur de nombreux sites (comme DIY Mania et OpenPilot), mais
nous n'avons malheureusement pas gardé trace de tous les sites
consultés (chose que nous regrettons parfois).


